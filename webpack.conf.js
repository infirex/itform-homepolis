var webpack = require('webpack');
var resolve = require('path').resolve;
var root = __dirname;

module.exports = {
  context: root,
  entry: {
    kupikaskoForm: './webpack.entry.js'
  },
  output: {
    path: resolve(root, 'dist'),
    publicPath: '/',
    library: '[name]',
    filename: "bundle.js",
    plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin()
    ]
  }
};
